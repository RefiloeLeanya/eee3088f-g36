# EEE3088F G36
This is a repository for EEE3088F course offered at UCT. The group members are: Kolisi Elvin (KLSELV001) - Power Module, Leanya Refiloe ()- Sensing, Melato Masasa - Micro-controller.
Aim of the project is to build an autonomous logging daughter-board for your STM32Discovery
Boards. The core requirements for it are:

● 1x digital sensor
● 1x analog sensor 
● An 18650 connector
● A Li-Ion battery charger
● Input voltage polarity protection
● Battery polarity protection
● Battery Under-voltage cutout protection
● Modularity between perceived elements to allow for bypassing to ensure you meet partial mark requirements in the event of module failure.
● USB micro connector, detect and input and have an onboard FTDI


```
cd existing_repo
git remote add origin https://gitlab.com/RefiloeLeanya/eee3088f-g36.git
git branch -M main
git push -uf origin main
```

## Collaborate with your team
```
Kolisi Elvin - Power Module
Leanya Refiloe - Sensing
Melato Masasa - Micro-controller
```

## Name
EEE3088F Eco Sensing Hat Project 

## Description
Due to high theft of equipment both from households and places of work. The HAT is the subsystem of the security alarm that monitors an unsupervised venue or household. The alarm system is a motion detection sensor, the sensor will emit rays from the transmitter to the receiver placed across in the venue. The alarm system is triggered when the rays from the transmitter to the receiver are obstructed. In addition, a temperature sensor is coupled to the alarm system to monitor any increase in temperatures in the venue. If an increase in temperature is detected, an alarm is triggered, and a green LED indicator is illuminated to indicate a high temperature that exceeds a pre-set temperature value.

## Installation
To use files .kicad_pro files use KiCad 6.0 to open them, download [Link Here](https://www.kicad.org/download/)

To use the STM32D, download [link here](https://www.st.com/en/development-tools/stm32cubeide.html) 

To configure the FTDI CH340G use this [link](https://www.arduined.eu/files/windows10/CH341SER.zip)

## Getting Started with Eco Sensor HAT

## Introduction 
In this guide you will be shown how to get started with using the Eco sensor HAT. This will include the cofiguring of the EEPROM, STM32D, and FTDI.

this will be done in two basic steps:
1. Setting up the EEPROM along with the STM32D
2. Setting up the FTDI and powering the sensor board.

After completing these steps, the sensor HAT will be ready to log the data into the EEPROM and send it to your computer via a type C USB cable.

## What you will need

1. STM32Discovery board.
2. Eco Sensor HAT.
3. Type C USB cable (For data transfer and reading and writting on STM32D, EEPROM, FTDI).
4. A laptop to view data logged (optional)
5. STM32cubeIDE (installed in your laptop)
6. CH341SER driver installed in your laptop
7. 18650 Li ion battery (for sensor HAT)

## Software Installations

If you do not have STM32cubeIDE installed in your laptop use this [link](https://www.st.com/en/development-tools/stm32cubeide.html) and also install [CH341SER](https://www.arduined.eu/files/windows10/CH341SER.zip) driver 

## Connecting the hardware

Mount the STM32Discovery board on the sensor HAT, the USB ports of the HAT and the STM32Discovery board must face in the same direction. Connect the type C USB to the sensor HAT and to ypu laptop. Connect the 18650 Li ion battery on the battery holder noting the polarity of the battery.  

## Software Program 

Open the installed STM32cubeIDE on laptop, select the STM32 type matching the one your are using. 
Includes:

Main
temp_read ();
    temp_read()
    Write_eeprom()
light_read ();
    light_read()
    read_eeprom()
    MX_GPIO_Init ()
        #Setup_GPIO

read_eeprom ();
    write_eeprom()
    MX_USART1_UART_Init ()
        #Setup USART1
write_eeprom ();
    write_eeprom()
    MX_I2C2_Init ()
        #Setup I2C

## License
MIT License

[Link text Here](https://choosealicense.com/licenses/mit/)


## Project status
PCB manufactured and tested.
